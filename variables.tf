provider "azurerm" {
  features {}
  subscription_id = "${var.subscription_id}"
  client_id = "${var.client_id}"
  client_secret = "${var.client_secret}"
  tenant_id = "${var.tenant_id}"
}

variable "subscription_id" {
description = "Enter subscription ID for provisioning resource in Azure"  
}

variable "client_id" {
description = "Enter Client ID for provisioning resource in Azure"  
}
variable "client_secret" {
description = "Enter Client Secret for provisioning resource in Azure"  
}
variable "tenant_id" {
description = "Enter tenant ID for provisioning resource in Azure"  
}

