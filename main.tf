
resource "azurerm_resource_group" "example" {
  name     = "rg-demo-caz001"
  location = "eastus"
}

resource "azurerm_virtual_network" "example" {
  name                = "vnet-caz001-eastus-hub-001"
  address_space       = ["10.11.0.0/20"]
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_subnet" "example" {
  name                 = "snet-caz001-eastus-shared-001"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefix       = "10.11.0.0/23"
}

resource "azurerm_subnet" "example1" {
  name                 = "snet-caz001-eastus-Firewall-001"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefix       = "10.11.2.0/23"
}


resource "azurerm_network_security_group" "example" {
  name                = "nsg-demo-hub-001"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  security_rule {
    name                       = "Test"
    priority                   = 500
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = azurerm_subnet.example.id
  network_security_group_id = azurerm_network_security_group.example.id
}
